local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup("plugins")

--setup for lazy vim plugin manager

require "mappings"

vim.opt.clipboard=unnamedplus

vim.cmd([[
set number
" tabs for spaces when tabbing:
colorscheme everforest
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set textwidth=120
set noswapfile

nnoremap <silent> <Esc><Esc> :noh<CR>:call clearmatches()<CR>
]])
