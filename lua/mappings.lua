local map = require("utils").map

map("n", "<F2>", ":echo 'current time is: ' . strftime('%c')<CR>")
map("i", "<C-q>", "<C-X><C-O>")
